//
//  PE_2.swift
//  PE_1
//
//  Created by Andrey Kindrat on 3/16/19.
//  Copyright © 2019 Andrey Kindrat. All rights reserved.
//

import Foundation


func pe_2f(){
    var sumForCalculation = 0
    var sum = 0
    var firstElement = 1
    var secondElement = 2
    
    while firstElement<4000000{
        if(firstElement%2==0){
            //print(firstElement)
            sum+=firstElement
        }
        sumForCalculation = firstElement+secondElement
        firstElement = secondElement
        secondElement = sumForCalculation
    }
    print("Sum of even Fibonacci numbers whos values do not exceed four million = \(sum)\n")
}

