//
//  PE_1.swift
//  PE_1
//
//  Created by Andrey Kindrat on 3/16/19.
//  Copyright © 2019 Andrey Kindrat. All rights reserved.
//

import Foundation

func pe_1f(){
var sum = 0
let firstNumber = 3
let secondNumber = 5

for count in 1...999{
    if(count%firstNumber == 0 || count%secondNumber == 0){
        sum+=count
    }
}
print("Sum of all the multiples of 3 or 5 below 1000 = \(sum)\n")
}
